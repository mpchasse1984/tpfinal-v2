document.addEventListener("DOMContentLoaded", function () {

    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 'auto',
        centeredSlides: true,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });


    console.log("Ça fonctionne!!!");

    var scrollButton = document.querySelector(".mon-icone");
    var intervalId = 0;

    function scrollStep(scrollStepHeight) {
        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scrollTo(0, window.pageYOffset - scrollStepHeight);
    }

    function scrollToTop(scrollStepHeight, delay)
    {
        if (scrollStepHeight <= 0)
        {
            alert("The specified scroll step height must be positive!");
        } else if (delay <= 0)
        {
            alert("The specified scroll delay must be positive!");
        }
        intervalId = setInterval(function ()
        {
            scrollStep(scrollStepHeight);
        }, delay);

    }

    scrollButton.addEventListener("click", function () {
        console.log('test');

        scrollToTop(50, 16.6);


    })





    var connexion = new MovieDb();

    console.log(document.location);

    if (document.location.pathname.search("fiche-film.html") > 0) {
//Dans la page fiche-film.html

        var params = (new URL(document.location)).searchParams;
        console.log(params.get("id"));

        connexion.requeteInfoFilm(params.get("id"));
    } else {
        //Dans la page index.html
        connexion.requeteDernierFilm();
        connexion.requeteFilmPopulaire();
        connexion.requeteCommentaires();
    }


});


class MovieDb {

    constructor() {

        this.APIkey = "84419d00a63011058a543fb9a83ddfd7";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;
        this.totalSwiper = 9;

        this.totalActeurs = 6;

        this.totalComments = 5;

    }
//----------------Now playing--------------------------------//
    requeteDernierFilm() {


        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true; --- Ne fonctionne pas de notre côté

        xhr.addEventListener("readystatechange", this.retourRequeteDernierFilm.bind(this));
        xhr.open("GET", this.baseURL + "movie/now_playing?language=" + this.lang + "&api_key=" + this.APIkey);


        xhr.send();
    }

    retourRequeteDernierFilm(event) {

        var target = event.currentTarget;

        var data;


        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText).results;


            this.afficheDernierFilm(data);
        }


    }

    afficheDernierFilm(data) {

        for (var i = 0; i < this.totalSwiper; i++) {

            //Clone de l'Article dans le template

            var unArticle = document.querySelector(".template>div.swiper-slide").cloneNode(true);


            unArticle.querySelector("h2").innerText = data[i].title;

            var uneImage = unArticle.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);

            var lienFilm = unArticle.querySelector("a");
            lienFilm.setAttribute("href", "fiche-film.html?id=" + data[i].id);

            unArticle.querySelector("p").innerText = "Note: "+data[i].vote_average;



            //Ajout du clone dans la liste du film
            document.querySelector("div.swiper-wrapper").appendChild(unArticle);

            // console.log(data[i].title);
        }

    }
    //----Most popular------------/
    requeteFilmPopulaire() {

        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true; --- Ne fonctionne pas de notre côté

        xhr.addEventListener("readystatechange", this.retourRequeteFilmPopulaire.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?"+"api_key=" + this.APIkey+"&language=" + this.lang + "&page=1");

        xhr.send();
    }

    retourRequeteFilmPopulaire(event) {

        var target = event.currentTarget;

        var data;


        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText).results;

            // console.log(data);

            this.afficheFilmPopulaire(data);
        }


    }

    afficheFilmPopulaire(data) {

        for (var i = 0; i < this.totalFilm; i++) {

            //Clone de l'Article dans le template

            var unArticle = document.querySelector(".template>article.film").cloneNode(true);


            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector(".vote").innerText = "Note: "+data[i].vote_average;
            unArticle.querySelector(".date").innerText ="Date de sortie: "+ data[i].release_date;

            if (data[i].overview == "") {
                unArticle.querySelector(".description").innerText = "Description à venir";
            } else {
                unArticle.querySelector(".description").innerText = data[i].overview;
            }


            var uneImage = unArticle.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);

            var lienFilm = unArticle.querySelector("a");
            lienFilm.setAttribute("href", "fiche-film.html?id=" + data[i].id);




            //Ajout du clone dans la liste du film
            document.querySelector("section.liste-films").appendChild(unArticle);

            // console.log(data[i].title);
        }

    }



    requeteInfoFilm(idFilm) {


        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }


    retourRequeteInfoFilm(event) {

        var target = event.currentTarget;

        var data;


        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText);

            console.log(data);
            // this.afficheDernierFilm(data);

            this.afficheInfoFilm(data);
        }


    }

    afficheInfoFilm(data) {

        document.querySelector("h1").innerText = data.title;
        var uneImage =document.querySelector(".affiche");
        uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[5] + data.poster_path);

        document.querySelector(".overview").innerText = data.overview;
        document.querySelector(".votefilm").innerText = "Note: "+data.vote_average;
        document.querySelector(".daterelease").innerText ="Date de sortie: "+ data.release_date;
        document.querySelector(".original-language").innerText ="Langue d'origine: "+ data.original_language;
        document.querySelector(".runtime").innerText ="Durée: "+ data.runtime+ " minutes";
        document.querySelector(".budget").innerText ="Budget: "+ data.budget+ "$";
        document.querySelector(".recette").innerText ="Recette: "+ data.revenue+ " $";
        document.querySelector(".castacteur").innerText ="Distribution: "+ data.name;

                //


        if (data.overview == "") {
            document.querySelector(".overview").innerText = "Description à venir";
        } else {
            document.querySelector(".overview").innerText = data.overview;
        }



        //appeler la requeteTeteAffiche Movie>get Credits 6 acteurs
        this.requeteInfoActeurs(data.id);

    }


    // Acteurs


    requeteInfoActeurs(idFilm) {


        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true; --- Ne fonctionne pas de notre côté

        xhr.addEventListener("readystatechange", this.retourRequeteInfoActeurs.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "/credits" + "?api_key=" + this.APIkey);


        xhr.send();
    }

    retourRequeteInfoActeurs(event) {

        var target = event.currentTarget;

        var data;


        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText).cast;


            this.afficheInfoActeurs(data);
        }


    }

    afficheInfoActeurs(data) {
        console.log(data);

        for (var i = 0; i < this.totalActeurs; i++) {

            // Clone de l'Article dans le template

            var unActeur = document.querySelector(".template>article.acteur").cloneNode(true);


            unActeur.querySelector("h3").innerText = data[i].name;

            var uneImage = unActeur.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurTeteAffiche[1] + data[i].profile_path);
            uneImage.setAttribute("alt",  data[i].name);


            // //Ajout du clone dans la liste du film
            document.querySelector("div.liste-acteurs").appendChild(unActeur);


        }


    }





}


